module bitbucket.org/olympiaschooldistrict/mailer

go 1.13

require (
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/xhit/go-simple-mail/v2 v2.3.0
)
