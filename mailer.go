package mailer

import (
	"time"

	"github.com/kelseyhightower/envconfig"
	mail "github.com/xhit/go-simple-mail/v2"
)

type SMTP struct {
	Port     int    `required:"true"`
	AuthUser string `required:"true" split_words:"true"`
	AuthPass string `required:"true" split_words:"true"`
	Host     string `required:"true"`
	server   *mail.SMTPServer
}

func NewFromEnv(prefix string) (*SMTP, error) {
	s := &SMTP{}
	err := envconfig.Process(prefix, s)
	if err != nil {
		return nil, err
	}
	server := mail.NewSMTPClient()
	server.Host = s.Host
	server.Port = s.Port
	server.Username = s.AuthUser
	server.Password = s.AuthPass

	server.Encryption = mail.EncryptionTLS

	server.SendTimeout = 10 * time.Second
	server.ConnectTimeout = 10 * time.Second
	server.KeepAlive = true
	s.server = server

	return s, nil
}

// SendMessage sends an email. html may be blank, all other parameters required
func (s *SMTP) SendMessage(from string, subject string, body string, html string, to ...string) error {
	m := mail.NewMSG()
	m.SetSender(from)
	m.SetSubject(subject)
	m.SetBody(mail.TextPlain, body)
	m.AddTo(to...)

	if len(html) != 0 {
		m.AddAlternative(mail.TextHTML, html)
	}

	smtpClient, err := s.server.Connect()
	if err != nil {
		return err
	}

	return m.Send(smtpClient)

}
