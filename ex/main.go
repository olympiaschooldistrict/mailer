package main

import (
	"log"
	"os"

	"bitbucket.org/olympiaschooldistrict/mailer"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	if len(os.Args) == 1 {
		log.Println("Please include test email adress in command line")
		return
	}
	to := os.Args[1:]

	s, err := mailer.NewFromEnv("")
	if err != nil {
		log.Fatal(err)
	}
	err = s.SendMessage(s.AuthUser, "Hello World", "We're doomed", "<p>We're</p><p>Doomed.</p>", to...)
	if err != nil {
		log.Fatal(err)
	}
}
